#ifndef OCL_EXTERNAL_INCLUDE
#define OCL_EXTERNAL_INCLUDE(x) x
#endif
OCL_EXTERNAL_INCLUDE(

kernel void force(
	global double4 f[],
	const global double4 x[],
	const ulong n)
{
	const int i = get_global_id(0);

	const double4 xi = x[i];

	double4 fi = 0;
	for(int j = 0; j < n; j++)
	{
		if(i != j)
		{
			const double4 xj = x[j];
			const double4 r = xj - xi;

			const double r2 = dot(r, r);
			const double r3 = r2 * sqrt(r2);

			fi += r/r3;
		}
	}

	f[i] = fi;
}

kernel void move(
	global double4 x[],
	global double4 v[],
	const global double4 f[],
	const double m, const double dt)
{
	const int i = get_global_id(0);

	const double4 a = f[i]/m;
	
	const double4 xx = x[i];
	const double4 vv = v[i];

	x[i] = xx + vv*dt + a*dt*dt/2;
	v[i] = vv + a*dt;
}

)