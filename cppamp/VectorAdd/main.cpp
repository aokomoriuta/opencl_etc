#include <iostream>
#include <algorithm>
#include <numeric>
#include <array>
#include <amp.h>

using Device = concurrency::accelerator;

// 一番最後のデバイスを既定デバイスに指定する
void SetDefaultDevice()
{
	const auto devices = concurrency::accelerator::get_all();
	const auto device = std::find_if(devices.crbegin(), devices.crend(), [](const Device& d)
	{
		return !(d.get_is_emulated());
	});

	if(device == devices.crend())
	{
		throw "Useful device not found";
	}

	concurrency::accelerator::set_default(device->get_device_path());
}

int main()
{
	constexpr unsigned int n = 10000;

	// ホスト側のデータ作成
	std::vector<int> a(n);
	std::vector<int> b(n);
	std::vector<int> c(n);
	std::iota(a.begin(), a.end(), 0);
	std::iota(b.rbegin(), b.rend(), n);

	// デバイスを設定
	SetDefaultDevice();

	// デバイス側メモリを作成
	concurrency::array_view<int> aa(n, a);
	concurrency::array_view<int> bb(n, b);
	concurrency::array_view<int> cc(n, c);

	// 計算実行
	concurrency::parallel_for_each(
		aa.get_extent(),
		[aa, bb, &cc] (const concurrency::index<1> idx) restrict(amp)
		{
			const int i = idx[0];
			cc[i] = aa[i] + bb[i];
		});

	// 必要なデータをデバイスから取得
	cc.synchronize();

	// 検算
	for(int i = 0; i < n; i++)
	{
		if(c[i] != a[i] + b[i])
		{
			std::cout << i << ": actual=" << c[i] << ", expected="<<  a[i] + b[i] << std::endl;
			return -1;
		}
	}
	std::cout << "OK" << std::endl;

	return 0;
}
