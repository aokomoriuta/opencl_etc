#include <iostream>
#include <amp.h>

int main()
{
#define output(name) << "    " #name ": " << device.get_##name() << std::endl
	std::cout << std::boolalpha;
	for(auto device : concurrency::accelerator::get_all())
	{
		std::wcout << device.get_description() << std::endl;
		std::cout
			output(version)
			output(is_emulated)
			output(is_debug)
			output(has_display)
			output(dedicated_memory)
			output(supports_limited_double_precision)
			output(supports_double_precision)
			output(supports_cpu_shared_memory);
	}
	return 0;
}
