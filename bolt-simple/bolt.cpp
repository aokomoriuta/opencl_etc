#pragma warning(push)
#pragma warning(disable : 4996)
#include <bolt/cl/sort.h>
#include <vector>
#include <algorithm>
#pragma warning(pop)

int main()
{
	// generate random data (on host)
	std::vector<int> a(129);
	std::generate(a.begin(), a.end(), rand);
	// sort, run on best device in the platform
	bolt::cl::sort(a.begin(), a.end());

	for(const auto& val : a)
	{
		std::cout << val << std::endl;
	}

	return 0;
}

