kernel void VectorAdd(
	global int dst[],
	global const int left[],
	global const int right[])
{
	const int i = get_global_id(0);
	dst[i] = left[i] + right[i];
}
