#ifndef OCL_EXTERNAL_INCLUDE
#define OCL_EXTERNAL_INCLUDE(x) x
#endif
OCL_EXTERNAL_INCLUDE(
kernel void GeoMean(
	global double dst[],
	global const double2 grid[],
	global const ulong gridCount[],
	const ulong gridStride,
	const double gridSize)
{
	const int i = get_global_id(0);
	const int j = get_global_id(1);

	const int index = get_global_size(1)*i + j;

	const ulong count = gridCount[index];

	if (count != 0)
	{
		double mul = 1;
		const double p = 1.0 / count;
		const double2 base = ((double2)(i, j) + 0.5)*gridSize;
		for (int k = 0; k < count; k++)
		{
			const double2 val = grid[gridStride*index + k];
			mul *= pow(distance(val, base), p);
		}
		dst[index] = mul;
	}
	else
	{
		dst[index] = 0;
	}
}

typedef struct __attribute__ ((packed)) ListItem
{
	double2 val;
	global struct ListItem* next;
} ListItem;

typedef global ListItem* ListIterator;

typedef struct __attribute__ ((packed))
{
	ListIterator first;
	ListIterator last;

	ulong count;
} Grid;


kernel void GeoMeanIndex(
	global double dst[],
	global const long indeces[],
	global const Grid grid[],
	global const ListItem items[],
	const ulong gridStride,
	const double gridSize)
{
	const int i = get_global_id(0);
	const int j = get_global_id(1);

	const int index = get_global_size(1)*i + j;

	const ulong count = grid[index].count;

	if (count != 0)
	{
		double mul = 1;
		const double p = 1.0 / count;
		const double2 base = ((double2)(i, j) + 0.5)*gridSize;
		for (int k = 0; k < count; k++)
		{
			const long ii = indeces[gridStride*index + k];
			const double2 val = items[ii].val;
			mul *= pow(distance(val, base), p);
		}
		dst[index] = mul;
	}
	else
	{
		dst[index] = 0;
	}
}

kernel void GeoMeanList(
	global double dst[],
	global const Grid grid[],
	const ulong gridStride,
	const double gridSize)
{
	const int i = get_global_id(0);
	const int j = get_global_id(1);

	const int index = get_global_size(1)*i + j;

	const ulong count = grid[index].count;

	if (count != 0)
	{
		double mul = 1;
		const double p = 1.0 / count;
		const double2 base = ((double2)(i, j) + 0.5)*gridSize;
		for (ListIterator it = grid[index].first; it != NULL; it = it->next)
		{
			const double2 val = it->val;
			mul *= pow(distance(val, base), p);
		}
		dst[index] = mul;
	}
	else
	{
		dst[index] = 0;
	}
}
)
