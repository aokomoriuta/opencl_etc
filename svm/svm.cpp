﻿#define _SCL_SECURE_NO_WARNINGS
#define __CL_ENABLE_EXCEPTIONS

#ifdef _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4996)
#endif
#include <iostream>
#include <memory>
#include <algorithm>
#include <chrono>
#include <random>
#include <list>
#include <CL/cl.hpp>
#include <omp.h>
#include <boost/pool/object_pool.hpp>
#include <boost/pool/simple_segregated_storage.hpp>
#ifdef _MSC_VER
#undef max
#pragma warning(pop)
#endif

#define OCL_EXTERNAL_INCLUDE(x) #x
static const char srcStr[] =
#include "ocl.cl"
;

class Timer
{
	typedef std::chrono::time_point<std::chrono::system_clock> time_point;

	time_point begin;

public:
	void Start()
	{
		this->begin = std::chrono::system_clock::now();
	}

	std::chrono::milliseconds Time()
	{
		const auto end = std::chrono::system_clock::now();
		return std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
	}
};

struct Point
{
	double x;
	double y;
};

typedef std::pair<std::ptrdiff_t, double> Result;

template<typename T>
struct ListItem
{
private:
	T val;
	ListItem* next;

	static ListItem* const LAST;

public:
	ListItem()
		: val(0), next(LAST){}

	ListItem(const T& v)
		:val(v), next(LAST){};

	void Init(const T& v)
	{
		val = v;
		next = LAST;
	}

	void Append(ListItem& n)
	{
		next = &n;
	}

	T Val() const
	{
		return val;
	}

	ListItem* Next()
	{
		return next;
	}

	ListItem& operator=(const ListItem& src)
	{
		val = src.val;
		next = src.next;

		return *this;
	}
};
template<typename T>
ListItem<T>* const ListItem<T>::LAST = nullptr;

template<typename T, typename ALLOCATOR>
class List
{
private:
	ListItem<T>* first;
	ListItem<T>* last;

	std::size_t count;

public:
	List()
		: first(nullptr), last(nullptr), count(0)
	{}

	void Init()
	{
		first = nullptr;
		last = nullptr;
		count = 0;
	}

	void Add(const T val)
	{
		ListItem<T>* const t = ALLOCATOR::Allocate(val);
		if (first == nullptr)
		{
			first = t;
			last = first;
		}
		else
		{
			last->Append(*t);
			last = t;
		}
		count++;
	}

	size_t Count() const
	{
		return count;
	}

	ListItem<T>* First()
	{
		return first;
	}

	~List()
	{
		for (ListItem<T>* it = first; it != nullptr;)
		{
			const auto next = it->Next();
			ALLOCATOR::Deallocate(it);
			it = next;
		}
	}
};

// CPU（ホスト側）
static Result MaxGeoMeanHost(const Point src[], const std::size_t n, const double size, const std::size_t gridCount)
{
	typedef std::list<Point> Grid;
	const std::size_t gridTotalCount = gridCount*gridCount;

	// グリッドに登録
	const double gridSize = size / gridCount;
	std::unique_ptr<Grid[]> grid(new Grid[gridTotalCount]);
	for (int i = 0; i < n; i++)
	{
		const auto& val = src[i];
		const std::size_t ii = static_cast<std::size_t>(std::floor(val.x / gridSize));
		const std::size_t jj = static_cast<std::size_t>(std::floor(val.y / gridSize));
		const std::size_t key = gridCount*ii + jj;

		grid[key].push_back(val);
	}

	// 各グリッドごとの相乗平均を計算
	std::unique_ptr<double[]> dst(new double[gridTotalCount]);
	for (int i = 0; i < gridCount; i++)
	{
		for (int j = 0; j < gridCount; j++)
		{
			const std::size_t index = gridCount*i + j;
			if (grid[index].size() != 0)
			{
				double mul = 1;
				const double p = 1.0 / grid[index].size();
				const double gridX = (i + 0.5)*gridSize;
				const double gridY = (j + 0.5)*gridSize;
				for (auto val : grid[index])
				{
					const double dx = val.x - gridX;
					const double dy = val.y - gridY;
					mul *= std::pow(dx*dx + dy*dy, p / 2);
				}
				dst[index] = mul;
			}
			else
			{
				dst[index] = 0;
			}
		}
	}

	// 最大値を求めて返す
	const double* result = std::max_element(dst.get(), dst.get() + gridTotalCount);
	return std::make_pair(result - dst.get(), *result);
}

// メモリプール版
static Result MaxGeoMeanHostPool(const Point src[], const std::size_t n, const double size, const std::size_t gridCount)
{
	static boost::object_pool<ListItem<Point>> pool(n);

	struct Allocator
	{
		static ListItem<Point>* Allocate(const Point& val)
		{
			return pool.construct(val);
		}
		static void Deallocate(ListItem<Point>* const)
		{
			// 最終的にpoolが消える時に全て消えるので不要
		}
	};
	typedef List<Point, Allocator> Grid;
	const std::size_t gridTotalCount = gridCount*gridCount;

	// グリッドに登録
	const double gridSize = size / gridCount;
	std::unique_ptr<Grid[]> grid(new Grid[gridTotalCount]);
	for (int i = 0; i < n; i++)
	{
		const auto& val = src[i];
		const std::size_t ii = static_cast<std::size_t>(std::floor(val.x / gridSize));
		const std::size_t jj = static_cast<std::size_t>(std::floor(val.y / gridSize));
		const std::size_t key = gridCount*ii + jj;

		grid[key].Add(val);
	}

	// 各グリッドごとの相乗平均を計算
	std::unique_ptr<double[]> dst(new double[gridTotalCount]);
	for (int i = 0; i < gridCount; i++)
	{
		for (int j = 0; j < gridCount; j++)
		{
			const std::size_t index = gridCount*i + j;
			if (grid[index].Count() != 0)
			{
				double mul = 1;
				const double p = 1.0 / grid[index].Count();
				const double gridX = (i + 0.5)*gridSize;
				const double gridY = (j + 0.5)*gridSize;
				for (auto it = grid[index].First(); it != nullptr; it = it->Next())
				{
					const auto& val = it->Val();
					const double dx = val.x - gridX;
					const double dy = val.y - gridY;
					mul *= std::pow(dx*dx + dy*dy, p / 2);
				}
				dst[index] = mul;
			}
			else
			{
				dst[index] = 0;
			}
		}
	}

	// 最大値を求めて返す
	const double* result = std::max_element(dst.get(), dst.get() + gridTotalCount);
	return std::make_pair(result - dst.get(), *result);
}

// OpenMP版
static Result MaxGeoMeanHostOmp(const Point src[], const std::size_t n, const double size, const std::size_t gridCount)
{
	static boost::object_pool<ListItem<Point>> pool(n);

	struct Allocator
	{
		static ListItem<Point>* Allocate(const Point& val)
		{
			return pool.construct(val);
		}
		static void Deallocate(ListItem<Point>* const)
		{
			// 最終的にpoolが消える時に全て消えるので不要
		}
	};
	typedef List<Point, Allocator> Grid;
	const std::size_t gridTotalCount = gridCount*gridCount;

	// グリッドに登録
	const double gridSize = size / gridCount;
	std::unique_ptr<Grid[]> grid(new Grid[gridTotalCount]);
	for (int i = 0; i < n; i++)
	{
		const auto& val = src[i];
		const std::size_t ii = static_cast<std::size_t>(std::floor(val.x / gridSize));
		const std::size_t jj = static_cast<std::size_t>(std::floor(val.y / gridSize));
		const std::size_t key = gridCount*ii + jj;

		grid[key].Add(val);
	}

	// 各グリッドごとの相乗平均を計算
	std::unique_ptr<double[]> dst(new double[gridTotalCount]);
	#pragma omp parallel for
	for (int i = 0; i < gridCount; i++)
	{
		for (int j = 0; j < gridCount; j++)
		{
			const std::size_t index = gridCount*i + j;
			if (grid[index].Count() != 0)
			{
				double mul = 1;
				const double p = 1.0 / grid[index].Count();
				const double gridX = (i + 0.5)*gridSize;
				const double gridY = (j + 0.5)*gridSize;
				for (auto it = grid[index].First(); it != nullptr; it = it->Next())
				{
					const auto& val = it->Val();
					const double dx = val.x - gridX;
					const double dy = val.y - gridY;
					mul *= std::pow(dx*dx + dy*dy, p / 2);
				}
				dst[index] = mul;
			}
			else
			{
				dst[index] = 0;
			}
		}
	}

	// 最大値を求めて返す
	const double* result = std::max_element(dst.get(), dst.get() + gridTotalCount);
	return std::make_pair(result - dst.get(), *result);
}

// OpenCLの通常版
static Result MaxGeoMeanOclNormal(const Point src[], const std::size_t n, const double size, const std::size_t gridCount,
	cl::Context& context, cl::CommandQueue& queue, cl::Kernel& kernel)
{
	static boost::object_pool<ListItem<Point>> pool(n);

	struct Allocator
	{
		static ListItem<Point>* Allocate(const Point& val)
		{
			return pool.construct(val);
		}
		static void Deallocate(ListItem<Point>* const)
		{
			// 最終的にpoolが消える時に全て消えるので不要
		}
	};
	typedef List<Point, Allocator> Grid;
	const std::size_t gridTotalCount = gridCount*gridCount;

	// グリッドに登録
	const double gridSize = size / gridCount;
	std::unique_ptr<Grid[]> grid(new Grid[gridTotalCount]);
	for (int i = 0; i < n; i++)
	{
		const auto& val = src[i];
		const std::size_t ii = static_cast<std::size_t>(std::floor(val.x / gridSize));
		const std::size_t jj = static_cast<std::size_t>(std::floor(val.y / gridSize));
		const std::size_t key = gridCount*ii + jj;

		grid[key].Add(val);
	}

	// 各グリッドごとの相乗平均を計算
	std::unique_ptr<double[]> dst(new double[gridTotalCount]);
	{
		// 各グリッドの有効要素数を数える（ついでに最大値を求めておく）
		std::unique_ptr<std::size_t[]> count(new std::size_t[gridTotalCount]);
		cl_ulong gridStride = 0;
		for (int i = 0; i < gridTotalCount; i++)
		{
			count[i] = grid[i].Count();
			gridStride = std::max(gridStride, count[i]);
		}

		// リストの配列を2次元配列に複製
		std::unique_ptr<Point[]> gridBuffer(new Point[gridTotalCount*gridStride]);
		for (int i = 0; i < gridTotalCount; i++)
		{
			auto it = grid[i].First();
			for (int j = 0; j < count[i]; j++)
			{
				gridBuffer[gridStride * i + j] = it->Val();
				it = it->Next();
			}
		}

		// ホスト->デバイス転送
		cl::Buffer bufferDst(context, CL_MEM_WRITE_ONLY, sizeof(cl_double)*gridTotalCount);
		cl::Buffer bufferGrid(context, CL_MEM_READ_ONLY, sizeof(Point)*gridTotalCount*gridStride);
		cl::Buffer bufferGridCount(context, CL_MEM_READ_ONLY, sizeof(cl_ulong)*gridTotalCount);

		queue.enqueueWriteBuffer(bufferGrid, CL_FALSE, 0, sizeof(Point)*gridTotalCount*gridStride, gridBuffer.get());
		queue.enqueueWriteBuffer(bufferGridCount, CL_FALSE, 0, sizeof(cl_ulong)*gridTotalCount, count.get());

		kernel.setArg(0, bufferDst);
		kernel.setArg(1, bufferGrid);
		kernel.setArg(2, bufferGridCount);
		kernel.setArg(3, gridStride);
		kernel.setArg(4, gridSize);

		// 実行
		queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(gridCount, gridCount));

		// デバイス->ホスト転送
		queue.enqueueReadBuffer(bufferDst, CL_TRUE, 0, sizeof(cl_double)*gridTotalCount, dst.get());
	}

	// 最大値を求めて返す
	const double* result = std::max_element(dst.get(), dst.get() + gridTotalCount);
	return std::make_pair(result - dst.get(), *result);
}

// OpenCLのUSE_HOST_PTR版
static Result MaxGeoMeanOclUseHostPtr(const Point src[], const std::size_t n, const double size, const std::size_t gridCount,
	cl::Context& context, cl::CommandQueue& queue, cl::Kernel& kernel)
{
	static boost::object_pool<ListItem<Point>> pool(n);

	struct Allocator
	{
		static ListItem<Point>* Allocate(const Point& val)
		{
			return pool.construct(val);
		}
		static void Deallocate(ListItem<Point>* const)
		{
			// 最終的にpoolが消える時に全て消えるので不要
		}
	};
	typedef List<Point, Allocator> Grid;
	const std::size_t gridTotalCount = gridCount*gridCount;

	// グリッドに登録
	const double gridSize = size / gridCount;
	std::unique_ptr<Grid[]> grid(new Grid[gridTotalCount]);
	for (int i = 0; i < n; i++)
	{
		const auto& val = src[i];
		const std::size_t ii = static_cast<std::size_t>(std::floor(val.x / gridSize));
		const std::size_t jj = static_cast<std::size_t>(std::floor(val.y / gridSize));
		const std::size_t key = gridCount*ii + jj;

		grid[key].Add(val);
	}

	// 各グリッドごとの相乗平均を計算
	std::unique_ptr<double[]> dst(new double[gridTotalCount]);
	{
		// 各グリッドの有効要素数を数える（ついでに最大値を求めておく）
		std::unique_ptr<std::size_t[]> count(new std::size_t[gridTotalCount]);
		cl_ulong gridStride = 0;
		for (int i = 0; i < gridTotalCount; i++)
		{
			count[i] = grid[i].Count();
			gridStride = std::max(gridStride, count[i]);
		}

		// リストの配列を2次元配列に複製
		std::unique_ptr<Point[]> gridBuffer(new Point[gridTotalCount*gridStride]);
		for (int i = 0; i < gridTotalCount; i++)
		{
			auto it = grid[i].First();
			for (int j = 0; j < count[i]; j++)
			{
				gridBuffer[gridStride * i + j] = it->Val();
				it = it->Next();
			}
		}

		// バッファー作成
		cl::Buffer bufferDst(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, sizeof(cl_double)*gridTotalCount, dst.get());
		cl::Buffer bufferGrid(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(Point)*gridTotalCount*gridStride, gridBuffer.get());
		cl::Buffer bufferGridCount(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(cl_ulong)*gridTotalCount, count.get());

		kernel.setArg(0, bufferDst);
		kernel.setArg(1, bufferGrid);
		kernel.setArg(2, bufferGridCount);
		kernel.setArg(3, gridStride);
		kernel.setArg(4, gridSize);

		// 実行
		cl::Event event;
		queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(gridCount, gridCount), cl::NullRange, nullptr, &event);
		event.wait();
	}

	// 最大値を求めて返す
	const double* result = std::max_element(dst.get(), dst.get() + gridTotalCount);
	return std::make_pair(result - dst.get(), *result);
}

// OpenCLのCopyHostPtr版
static Result MaxGeoMeanOclCopyHostPtr(const Point src[], const std::size_t n, const double size, const std::size_t gridCount,
	cl::Context& context, cl::CommandQueue& queue, cl::Kernel& kernel)
{
	static boost::object_pool<ListItem<Point>> pool(n);

	struct Allocator
	{
		static ListItem<Point>* Allocate(const Point& val)
		{
			return pool.construct(val);
		}
		static void Deallocate(ListItem<Point>* const)
		{
			// 最終的にpoolが消える時に全て消えるので不要
		}
	};
	typedef List<Point, Allocator> Grid;
	const std::size_t gridTotalCount = gridCount*gridCount;

	// グリッドに登録
	const double gridSize = size / gridCount;
	std::unique_ptr<Grid[]> grid(new Grid[gridTotalCount]);
	for (int i = 0; i < n; i++)
	{
		const auto& val = src[i];
		const std::size_t ii = static_cast<std::size_t>(std::floor(val.x / gridSize));
		const std::size_t jj = static_cast<std::size_t>(std::floor(val.y / gridSize));
		const std::size_t key = gridCount*ii + jj;

		grid[key].Add(val);
	}

	// 各グリッドごとの相乗平均を計算
	std::unique_ptr<double[]> dst(new double[gridTotalCount]);
	{
		// 各グリッドの有効要素数を数える（ついでに最大値を求めておく）
		std::unique_ptr<std::size_t[]> count(new std::size_t[gridTotalCount]);
		cl_ulong gridStride = 0;
		for (int i = 0; i < gridTotalCount; i++)
		{
			count[i] = grid[i].Count();
			gridStride = std::max(gridStride, count[i]);
		}

		// リストの配列を2次元配列に複製
		std::unique_ptr<Point[]> gridBuffer(new Point[gridTotalCount*gridStride]);
		for (int i = 0; i < gridTotalCount; i++)
		{
			auto it = grid[i].First();
			for (int j = 0; j < count[i]; j++)
			{
				gridBuffer[gridStride * i + j] = it->Val();
				it = it->Next();
			}
		}

		// バッファー作成
		cl::Buffer bufferDst(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, sizeof(cl_double)*gridTotalCount, dst.get());
		cl::Buffer bufferGrid(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(Point)*gridTotalCount*gridStride, gridBuffer.get());
		cl::Buffer bufferGridCount(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(cl_ulong)*gridTotalCount, count.get());

		kernel.setArg(0, bufferDst);
		kernel.setArg(1, bufferGrid);
		kernel.setArg(2, bufferGridCount);
		kernel.setArg(3, gridStride);
		kernel.setArg(4, gridSize);

		// 実行
		cl::Event event;
		queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(gridCount, gridCount), cl::NullRange, nullptr, &event);
		event.wait();
	}

	// 最大値を求めて返す
	const double* result = std::max_element(dst.get(), dst.get() + gridTotalCount);
	return std::make_pair(result - dst.get(), *result);
}

// OpenCLのMapBuffer版
static Result MaxGeoMeanOclMapBuffer(const Point src[], const std::size_t n, const double size, const std::size_t gridCount,
	cl::Context& context, cl::CommandQueue& queue, cl::Kernel& kernel)
{
	static boost::object_pool<ListItem<Point>> pool(n);

	struct Allocator
	{
		static ListItem<Point>* Allocate(const Point& val)
		{
			return pool.construct(val);
		}
		static void Deallocate(ListItem<Point>* const)
		{
			// 最終的にpoolが消える時に全て消えるので不要
		}
	};
	typedef List<Point, Allocator> Grid;
	const std::size_t gridTotalCount = gridCount*gridCount;

	// グリッドに登録
	const double gridSize = size / gridCount;
	std::unique_ptr<Grid[]> grid(new Grid[gridTotalCount]);
	for (int i = 0; i < n; i++)
	{
		const auto& val = src[i];
		const std::size_t ii = static_cast<std::size_t>(std::floor(val.x / gridSize));
		const std::size_t jj = static_cast<std::size_t>(std::floor(val.y / gridSize));
		const std::size_t key = gridCount*ii + jj;

		grid[key].Add(val);
	}

	// 各グリッドごとの相乗平均を計算

	// dstにバッファーを確保
	cl::Buffer bufferDst(context, CL_MEM_ALLOC_HOST_PTR, sizeof(cl_double)*gridTotalCount);
	{
		// countのバッファーを作成してホストから書けるようにする
		cl::Buffer bufferGridCount(context, CL_MEM_ALLOC_HOST_PTR, sizeof(cl_ulong)*gridTotalCount);
		cl::Event eventMapGridCount;
		std::size_t* const count = static_cast<std::size_t*>(queue.enqueueMapBuffer(bufferGridCount, CL_FALSE, CL_MAP_WRITE, 0, sizeof(cl_ulong)*gridTotalCount, nullptr, &eventMapGridCount));

		// 各グリッドの最大有効要素数を求める
		cl_ulong gridStride = 0;
		for (int i = 0; i < gridTotalCount; i++)
		{
			gridStride = std::max(gridStride, grid[i].Count());
		}

		// gridのバッファーを作成してホストから書けるようにする
		cl::Buffer bufferGrid(context, CL_MEM_ALLOC_HOST_PTR, sizeof(Point)*gridTotalCount*gridStride);
		cl::Event eventMapGrid;
		Point* const gridBuffer = static_cast<Point*>(queue.enqueueMapBuffer(bufferGrid, CL_FALSE, CL_MAP_WRITE, 0, sizeof(Point)*gridTotalCount*gridStride, nullptr, &eventMapGrid));

		// 各グリッドの有効要素数を数える
		eventMapGridCount.wait();
		for (int i = 0; i < gridTotalCount; i++)
		{
			count[i] = grid[i].Count();
		}
		// ホスト側からのグリッドの有効要素数の操作は完了した（ホスト->デバイス転送）
		queue.enqueueUnmapMemObject(bufferGridCount, count);

		// リストの配列を2次元配列に複製
		eventMapGrid.wait();
		for (int i = 0; i < gridTotalCount; i++)
		{
			auto it = grid[i].First();
			for (int j = 0; j < count[i]; j++)
			{
				gridBuffer[gridStride * i + j] = it->Val();
				it = it->Next();
			}
		}
		// ホスト側からのグリッドの操作は完了した（ホスト->デバイス転送）
		queue.enqueueUnmapMemObject(bufferGrid, gridBuffer);

		kernel.setArg(0, bufferDst);
		kernel.setArg(1, bufferGrid);
		kernel.setArg(2, bufferGridCount);
		kernel.setArg(3, gridStride);
		kernel.setArg(4, gridSize);

		// 実行
		queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(gridCount, gridCount));
	}

	// dstのデバイスメモリをホストから読めるようにする
	double* const dst = reinterpret_cast<double*>(queue.enqueueMapBuffer(bufferDst, CL_TRUE, CL_MAP_READ, 0, sizeof(cl_double)*gridTotalCount));

	// 最大値を求めて返す
	const double* result = std::max_element(dst, dst + gridTotalCount);

	// ホストはもうdstを使わない（解放完了したかは気にしないので、待たなくてもいい）
	queue.enqueueUnmapMemObject(bufferDst, dst);

	return std::make_pair(result - dst, *result);
}

// OpenCLのメモリプールもMapBuffer版
static Result MaxGeoMeanOclMapPool(const Point src[], const std::size_t n, const double size, const std::size_t gridCount,
	cl::Context& context, cl::CommandQueue& queue, cl::Kernel& kernel)
{
	// メモリプール用のバッファーを確保してホストから読み書きできるようにする
	cl::Buffer bufferPool(context, CL_MEM_ALLOC_HOST_PTR, sizeof(ListItem<Point>)*n);
	ListItem<Point>* const poolSource = static_cast<ListItem<Point>*>(queue.enqueueMapBuffer(bufferPool, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, sizeof(ListItem<Point>)*n));
	static boost::simple_segregated_storage<std::size_t>* pool;
	pool = new boost::simple_segregated_storage<std::size_t>();
	pool->add_block(poolSource, sizeof(ListItem<Point>)*n, sizeof(ListItem<Point>));

	struct Allocator
	{
		static ListItem<Point>* Allocate(const Point& val)
		{
			const auto ptr = static_cast<ListItem<Point>*>(pool->malloc());
			ptr->Init(val);
			return ptr;
		}
		static void Deallocate(ListItem<Point>* const)
		{
			// 最終的にpoolSourceが消える時に全て消えるので不要
		}
	};
	typedef List<Point, Allocator> Grid;
	const std::size_t gridTotalCount = gridCount*gridCount;

	// グリッドのバッファーを作成してホストから読み書きできるようにする
	cl::Buffer bufferGrid(context, CL_MEM_ALLOC_HOST_PTR, sizeof(Grid)*gridTotalCount);
	Grid* const grid = static_cast<Grid*>(queue.enqueueMapBuffer(bufferGrid, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, sizeof(Grid)*gridTotalCount));
	for (int i = 0; i < gridTotalCount; i++)
	{
		grid[i].Init();
	}

	// グリッドに登録
	const double gridSize = size / gridCount;
	for (int i = 0; i < n; i++)
	{
		const auto& val = src[i];
		const std::size_t ii = static_cast<std::size_t>(std::floor(val.x / gridSize));
		const std::size_t jj = static_cast<std::size_t>(std::floor(val.y / gridSize));
		const std::size_t key = gridCount*ii + jj;

		grid[key].Add(val);
	}

	// 各グリッドごとの相乗平均を計算

	// dstにバッファーを確保
	cl::Buffer bufferDst(context, CL_MEM_ALLOC_HOST_PTR, sizeof(cl_double)*gridTotalCount);
	{
		// 各グリッドの最大有効要素数を求める
		cl_ulong gridStride = 0;
		for (int i = 0; i < gridTotalCount; i++)
		{
			gridStride = std::max(gridStride, grid[i].Count());
		}

		// indexのバッファーを作成してホストから書けるようにする
		cl::Buffer bufferIndex(context, CL_MEM_ALLOC_HOST_PTR, sizeof(cl_long)*gridTotalCount*gridStride);
		cl::Event eventMapIndex;
		std::ptrdiff_t* const indeces = static_cast<std::ptrdiff_t*>(queue.enqueueMapBuffer(bufferIndex, CL_FALSE, CL_MAP_WRITE, 0, sizeof(cl_long)*gridTotalCount*gridStride, nullptr, &eventMapIndex));

		// リストの配列を2次元配列に複製
		eventMapIndex.wait();
		for (int i = 0; i < gridTotalCount; i++)
		{
			auto it = grid[i].First();
			for (int j = 0; j < grid[i].Count(); j++)
			{
				const std::ptrdiff_t index = it - poolSource;
				indeces[gridStride * i + j] = index;
				it = it->Next();
			}
		}

		// ホスト側からのグリッドの操作は完了した（ホスト->デバイス転送）
		queue.enqueueUnmapMemObject(bufferIndex, indeces);
		queue.enqueueUnmapMemObject(bufferPool, poolSource);
		queue.enqueueUnmapMemObject(bufferGrid, grid);

		kernel.setArg(0, bufferDst);
		kernel.setArg(1, bufferIndex);
		kernel.setArg(2, bufferGrid);
		kernel.setArg(3, bufferPool);
		kernel.setArg(4, gridStride);
		kernel.setArg(5, gridSize);

		// 実行
		queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(gridCount, gridCount));

		// もうpool使わないので、次回以降呼ばれた時のために解放しておく（カーネル実行時間に隠蔽）
		delete pool;
	}

	// dstのデバイスメモリをホストから読めるようにする
	double* const dst = reinterpret_cast<double*>(queue.enqueueMapBuffer(bufferDst, CL_TRUE, CL_MAP_READ, 0, sizeof(cl_double)*gridTotalCount));

	// 最大値を求めて返す
	const double* result = std::max_element(dst, dst + gridTotalCount);

	// ホストはもうdstを使わない（解放完了したかは気にしないので、待たなくてもいい）
	queue.enqueueUnmapMemObject(bufferDst, dst);

	return std::make_pair(result - dst, *result);
}

// OpenCLのSVMCoarse版
static Result MaxGeoMeanOclSVMCoarse(const Point src[], const std::size_t n, const double size, const std::size_t gridCount,
	cl::Context& context, cl::CommandQueue& queue, cl::Kernel& kernel)
{
	// SVMを割り当ててホストから読み書きできるようにする
	ListItem<Point>* const poolSource = static_cast<ListItem<Point>*>(clSVMAlloc(context(), CL_MEM_READ_WRITE, sizeof(ListItem<Point>)*n, 0));
	clEnqueueSVMMap(queue(), CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, poolSource, sizeof(ListItem<Point>)*n, 0, nullptr, nullptr);
	static boost::simple_segregated_storage<std::size_t>* pool;
	pool = new boost::simple_segregated_storage<std::size_t>();
	pool->add_block(poolSource, sizeof(ListItem<Point>)*n, sizeof(ListItem<Point>));

	struct Allocator
	{
		static ListItem<Point>* Allocate(const Point& val)
		{
			const auto ptr = static_cast<ListItem<Point>*>(pool->malloc());
			ptr->Init(val);
			return ptr;
		}
		static void Deallocate(ListItem<Point>* const)
		{
			// 最終的にpoolSourceが消える時に全て消えるので不要
		}
	};
	typedef List<Point, Allocator> Grid;
	const std::size_t gridTotalCount = gridCount*gridCount;

	// グリッドのバッファーを作成してホストから読み書きできるようにする
	cl::Buffer bufferGrid(context, CL_MEM_ALLOC_HOST_PTR, sizeof(Grid)*gridTotalCount);
	Grid* const grid = static_cast<Grid*>(queue.enqueueMapBuffer(bufferGrid, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, sizeof(Grid)*gridTotalCount));
	for (int i = 0; i < gridTotalCount; i++)
	{
		grid[i].Init();
	}

	// グリッドに登録
	const double gridSize = size / gridCount;
	for (int i = 0; i < n; i++)
	{
		const auto& val = src[i];
		const std::size_t ii = static_cast<std::size_t>(std::floor(val.x / gridSize));
		const std::size_t jj = static_cast<std::size_t>(std::floor(val.y / gridSize));
		const std::size_t key = gridCount*ii + jj;

		grid[key].Add(val);
	}

	// ホスト側でグリッドに追加＆修正することはない
	cl::Event event;
	clEnqueueSVMUnmap(queue(), poolSource, 0, nullptr, &(event()));

	// 各グリッドごとの相乗平均を計算

	// dstにバッファーを確保
	cl::Buffer bufferDst(context, CL_MEM_ALLOC_HOST_PTR, sizeof(cl_double)*gridTotalCount);
	{
		// 各グリッドの最大有効要素数を求める
		cl_ulong gridStride = 0;
		for (int i = 0; i < gridTotalCount; i++)
		{
			gridStride = std::max(gridStride, grid[i].Count());
		}

		// ホスト側からのグリッドの操作は完了した
		queue.enqueueUnmapMemObject(bufferGrid, grid);

		kernel.setArg(0, bufferDst);
		kernel.setArg(1, bufferGrid);
		kernel.setArg(2, gridStride);
		kernel.setArg(3, gridSize);

		// メモリプールの中身は引数に直接渡す必要がないので、別に設定する
		clSetKernelExecInfo(kernel(), CL_KERNEL_EXEC_INFO_SVM_PTRS, sizeof(poolSource), &poolSource);

		// 実行
		event.wait();
		queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(gridCount, gridCount), cl::NullRange, nullptr, &event);

		// もうpool使わないので、次回以降呼ばれた時のために解放しておく（カーネル実行時間に隠蔽）
		delete pool;

		event.wait();
	}

	// dstのデバイスメモリをホストから読めるようにする
	double* const dst = reinterpret_cast<double*>(queue.enqueueMapBuffer(bufferDst, CL_TRUE, CL_MAP_READ, 0, sizeof(cl_double)*gridTotalCount));

	// 最大値を求めて返す
	const double* result = std::max_element(dst, dst + gridTotalCount);

	// ホストはもうdstを使わない（解放完了したかは気にしないので、待たなくてもいい）
	queue.enqueueUnmapMemObject(bufferDst, dst);
	clSVMFree(context(), poolSource);

	return std::make_pair(result - dst, *result);
}

// OpenCLのSVMFine版
static Result MaxGeoMeanOclSVMFine(const Point src[], const std::size_t n, const double size, const std::size_t gridCount,
	cl::Context& context, cl::CommandQueue& queue, cl::Kernel& kernel)
{
	// SVMを割り当ててホストから読み書きできるようにする
	ListItem<Point>* const poolSource = static_cast<ListItem<Point>*>(clSVMAlloc(context(), CL_MEM_READ_WRITE | CL_MEM_SVM_FINE_GRAIN_BUFFER, sizeof(ListItem<Point>)*n, 0));
	static boost::simple_segregated_storage<std::size_t>* pool;
	pool = new boost::simple_segregated_storage<std::size_t>();
	pool->add_block(poolSource, sizeof(ListItem<Point>)*n, sizeof(ListItem<Point>));

	struct Allocator
	{
		static ListItem<Point>* Allocate(const Point& val)
		{
			const auto ptr = static_cast<ListItem<Point>*>(pool->malloc());
			ptr->Init(val);
			return ptr;
		}
		static void Deallocate(ListItem<Point>* const)
		{
			// 最終的にpoolSourceが消える時に全て消えるので不要
		}
	};
	typedef List<Point, Allocator> Grid;
	const std::size_t gridTotalCount = gridCount*gridCount;

	// グリッドのバッファーを作成してホストから読み書きできるようにする
	cl::Buffer bufferGrid(context, CL_MEM_ALLOC_HOST_PTR, sizeof(Grid)*gridTotalCount);
	Grid* const grid = static_cast<Grid*>(queue.enqueueMapBuffer(bufferGrid, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, sizeof(Grid)*gridTotalCount));
	for (int i = 0; i < gridTotalCount; i++)
	{
		grid[i].Init();
	}

	// グリッドに登録
	const double gridSize = size / gridCount;
	for (int i = 0; i < n; i++)
	{
		const auto& val = src[i];
		const std::size_t ii = static_cast<std::size_t>(std::floor(val.x / gridSize));
		const std::size_t jj = static_cast<std::size_t>(std::floor(val.y / gridSize));
		const std::size_t key = gridCount*ii + jj;

		grid[key].Add(val);
	}

	// 各グリッドごとの相乗平均を計算

	// dstにバッファーを確保
	cl::Buffer bufferDst(context, CL_MEM_ALLOC_HOST_PTR, sizeof(cl_double)*gridTotalCount);
	{
		// 各グリッドの最大有効要素数を求める
		cl_ulong gridStride = 0;
		for (int i = 0; i < gridTotalCount; i++)
		{
			gridStride = std::max(gridStride, grid[i].Count());
		}

		// ホスト側からのグリッドの操作は完了した
		queue.enqueueUnmapMemObject(bufferGrid, grid);

		kernel.setArg(0, bufferDst);
		kernel.setArg(1, bufferGrid);
		kernel.setArg(2, gridStride);
		kernel.setArg(3, gridSize);

		// メモリプールの中身は引数に直接渡す必要がないので、別に設定する
		clSetKernelExecInfo(kernel(), CL_KERNEL_EXEC_INFO_SVM_PTRS, sizeof(poolSource), &poolSource);

		// 実行
		cl::Event event;
		queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(gridCount, gridCount), cl::NullRange, nullptr, &event);
		event.wait();

		// もうpool使わないので、次回以降呼ばれた時のために解放しておく（カーネル実行時間に隠蔽）
		delete pool;
	}

	// dstのデバイスメモリをホストから読めるようにする
	double* const dst = reinterpret_cast<double*>(queue.enqueueMapBuffer(bufferDst, CL_TRUE, CL_MAP_READ, 0, sizeof(cl_double)*gridTotalCount));

	// 最大値を求めて返す
	const double* result = std::max_element(dst, dst + gridTotalCount);

	// ホストはもうdstを使わない（解放完了したかは気にしないので、待たなくてもいい）
	queue.enqueueUnmapMemObject(bufferDst, dst);
	clSVMFree(context(), poolSource);

	return std::make_pair(result - dst, *result);
}

int main()
{
	std::cout << "= SVM =" << std::endl;
	const std::size_t n = 10 * 1000 * 1000;
	const std::size_t gridCount = 1000;
	std::cout << "PointCount:" << n << std::endl;
	std::cout << "GridCount :" << gridCount << std::endl;

	#pragma omp parallel
	#pragma omp master
	{
		std::cout << "OpenMP: " << omp_get_num_threads() << "threads" << std::endl;
	}

	// デモ用入力値（0-√nの一様分布）
	const double size = std::sqrt(n);
	std::mt19937 rd(713);
	std::uniform_real_distribution<double> randomGenerator(0.0001, size);
	std::unique_ptr<Point[]> src(new Point[n]);
	for (int i = 0; i < n; i++)
	{
		const double x = randomGenerator(rd);
		const double y = randomGenerator(rd);
		src[i].x = x;
		src[i].y = y;
	}

	std::cout << "== Time ==" << std::endl;
	Timer timer;

	// CPU（ホスト側）
	Result resultHost;
	{
		std::cout << "Host               : " << std::flush;
		timer.Start();

		resultHost = MaxGeoMeanHost(src.get(), n, size, gridCount);

		const auto time = timer.Time();
		std::cout << time.count() << "[ms]" << std::endl;
	}

	// メモリプール版
	Result resultHostPool;
	{
		std::cout << "Host Pool          : " << std::flush;
		timer.Start();

		resultHostPool = MaxGeoMeanHostPool(src.get(), n, size, gridCount);

		const auto time = timer.Time();
		std::cout << time.count() << "[ms]" << std::endl;
	}

	// OpenMP版
	Result resultHostOmp;
	{
		std::cout << "Host OpenMP        : " << std::flush;
		timer.Start();

		resultHostOmp = MaxGeoMeanHostOmp(src.get(), n, size, gridCount);

		const auto time = timer.Time();
		std::cout << time.count() << "[ms]" << std::endl;
	}

	// OpenCL
	Result resultOclNormal;
	Result resultOclUseHostPtr;
	Result resultOclCopyHostPtr;
	Result resultOclMapBuffer;
	Result resultOclMapPool;
	Result resultOclSVMCoarse;
	Result resultOclSVMFine;
	{
		// プラットフォーム取得（複数ある場合は一番最後）
		std::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);
		const auto& platform = *(platforms.rbegin());

		// デバイスを取得（複数ある場合は一番最後）
		std::vector<cl::Device> devices;
		platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
		const auto& device = *(devices.rbegin());

		// コンテキスト作成
		cl::Context context(device);

		// プログラムの作成＆ビルド
		cl::Program program(context, srcStr);
		try
		{
			program.build(devices, "-cl-std=CL2.0");
		}
		// OpenCL例外があった場合
		catch (cl::Error error)
		{
			// ビルドログを表示
			std::cout << "Build error #" << error.err() << " @ " << error.what() << std::endl;
			std::cout << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << std::endl;
#ifdef _MSC_VER
			system("pause");
#endif
			return -1;
		}

		// カーネルを作成
		const std::string KERNEL_FUNCTION_NAME_GEOMEAN = "GeoMean";
		const std::string KERNEL_FUNCTION_NAME_GEOMEANINDEX = "GeoMeanIndex";
		const std::string KERNEL_FUNCTION_NAME_GEOMEANLIST = "GeoMeanList";
		cl::Kernel kernelGeoMean(program, KERNEL_FUNCTION_NAME_GEOMEAN.c_str());
		cl::Kernel kernelGeoMeanIndex(program, KERNEL_FUNCTION_NAME_GEOMEANINDEX.c_str());
		cl::Kernel kernelGeoMeanList(program, KERNEL_FUNCTION_NAME_GEOMEANLIST.c_str());

		// キュー作成
		cl::CommandQueue queue(context, device);

		// 通常版
		std::cout << "OpenCL Normal      : " << std::flush;
		queue.finish();
		{
			timer.Start();

			resultOclNormal = MaxGeoMeanOclNormal(src.get(), n, size, gridCount,
				context, queue, kernelGeoMean);

			const auto time = timer.Time();
			std::cout << time.count() << "[ms]" << std::endl;
		}

		// USE_HOST_PTR版
		std::cout << "OpenCL UseHostPtr  : " << std::flush;
		queue.finish();
		{
			timer.Start();

			resultOclUseHostPtr = MaxGeoMeanOclUseHostPtr(src.get(), n, size, gridCount,
				context, queue, kernelGeoMean);

			const auto time = timer.Time();
			std::cout << time.count() << "[ms]" << std::endl;
		}

		// COPY_HOST_PTR版
		std::cout << "OpenCL CopyHostPtr : " << std::flush;
		queue.finish();
		{
			timer.Start();

			resultOclCopyHostPtr = MaxGeoMeanOclCopyHostPtr(src.get(), n, size, gridCount,
				context, queue, kernelGeoMean);

			const auto time = timer.Time();
			std::cout << time.count() << "[ms]" << std::endl;
		}

		// MapBuffer版
		std::cout << "OpenCL MapBuffer   : " << std::flush;
		queue.finish();
		{
			timer.Start();

			resultOclMapBuffer = MaxGeoMeanOclMapBuffer(src.get(), n, size, gridCount,
				context, queue, kernelGeoMean);

			const auto time = timer.Time();
			std::cout << time.count() << "[ms]" << std::endl;
		}

		// MapPool版
		std::cout << "OpenCL MapPool     : " << std::flush;
		queue.finish();
		{
			timer.Start();

			resultOclMapPool = MaxGeoMeanOclMapPool(src.get(), n, size, gridCount,
				context, queue, kernelGeoMeanIndex);

			const auto time = timer.Time();
			std::cout << time.count() << "[ms]" << std::endl;
		}

		// SVMのサポート状況を取得
		cl_device_svm_capabilities svmCap;
		device.getInfo(CL_DEVICE_SVM_CAPABILITIES, &svmCap);
		const auto coarseSupported = ((svmCap&CL_DEVICE_SVM_COARSE_GRAIN_BUFFER) != 0);
		const auto fineSupported = ((svmCap&CL_DEVICE_SVM_FINE_GRAIN_BUFFER) != 0);

		// SVMCoarse版
		std::cout << "OpenCL SVMCoarse   : " << std::flush;
		queue.finish();
		{
			if (coarseSupported)
			{
				timer.Start();

				resultOclSVMCoarse = MaxGeoMeanOclSVMCoarse(src.get(), n, size, gridCount,
					context, queue, kernelGeoMeanList);

				const auto time = timer.Time();
				std::cout << time.count() << "[ms]" << std::endl;
			}
			else
			{
				std::cout << "Not supported" << std::endl;
			}
		}

		// SVMFine版
		std::cout << "OpenCL SVMFine     : " << std::flush;
		queue.finish();
		{
			if (fineSupported)
			{
				timer.Start();

				resultOclSVMFine = MaxGeoMeanOclSVMFine(src.get(), n, size, gridCount,
					context, queue, kernelGeoMeanList);

				const auto time = timer.Time();
				std::cout << time.count() << "[ms]" << std::endl;
			}
			else
			{
				std::cout << "Not supported" << std::endl;
			}
		}


		std::cout
			<< "== OpenCL info ==" << std::endl
			<< "=== Platform : " << platform() << " ===" << std::endl
			<< "Name    : " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl
			<< "Vendor  : " << platform.getInfo<CL_PLATFORM_VENDOR>() << std::endl
			<< "Version : " << platform.getInfo<CL_PLATFORM_VERSION>() << std::endl
			<< "=== Device : " << device() << " ===" << std::endl
			<< "Name              : " << device.getInfo<CL_DEVICE_NAME>() << std::endl
			<< "Vendor            : " << device.getInfo<CL_DEVICE_VENDOR>() << " (ID:" << device.getInfo<CL_DEVICE_VENDOR_ID>() << ")" << std::endl
			<< "Version           : " << device.getInfo<CL_DEVICE_VERSION>() << std::endl
			<< "Driver version    : " << device.getInfo<CL_DRIVER_VERSION>() << std::endl
			<< "OpenCL C version  : " << device.getInfo<CL_DEVICE_OPENCL_C_VERSION>() << std::endl
			<< "SVM Coarse Grain  : " << (coarseSupported ? "Yes" : "No") << std::endl
			<< "SVM Fine Grain    : " << (fineSupported ? "Yes" : "No") << std::endl;
	}

	// エラーチェック
	std::cout << "== Result ==" << std::endl
		<< "Host               : " << resultHost.second << " @" << resultHost.first << std::endl
		<< "Host Pool          : " << resultHostPool.second << " @" << resultHostPool.first << std::endl
		<< "Host OpenMP        : " << resultHostOmp.second << " @" << resultHostOmp.first << std::endl
		<< "OpenCL Normal      : " << resultOclNormal.second << " @" << resultOclNormal.first << std::endl
		<< "OpenCL UseHostPtr  : " << resultOclUseHostPtr.second << " @" << resultOclUseHostPtr.first << std::endl
		<< "OpenCL CopyHostPtr : " << resultOclCopyHostPtr.second << " @" << resultOclCopyHostPtr.first << std::endl
		<< "OpenCL MapBuffer   : " << resultOclMapBuffer.second << " @" << resultOclMapBuffer.first << std::endl
		<< "OpenCL MapPool     : " << resultOclMapPool.second << " @" << resultOclMapPool.first << std::endl
		<< "OpenCL SVMCoarse   : " << resultOclSVMCoarse.second << " @" << resultOclSVMCoarse.first << std::endl
		<< "OpenCL SVMFine     : " << resultOclSVMFine.second << " @" << resultOclSVMFine.first << std::endl;

#ifdef _MSC_VER
	system("pause");
#endif
	return 0;
}
